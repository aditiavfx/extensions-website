from files.validators import ManifestValidator


def clean_json_dictionary_from_optional_fields(data):
    """Keep optional values outside the dictionary

    Remove null entries, as well as empty lists, while keeping strings ""
    around for optional fields.
    """
    mandatory_fields = ManifestValidator.mandatory_fields
    return {
        key: value
        for key, value in data.items()
        if value is not None and (value != [] or isinstance(value, str) or key in mandatory_fields)
    }
