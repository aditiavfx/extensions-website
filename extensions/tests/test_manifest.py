from django.test import TestCase
from django.urls import reverse
from django.core.exceptions import ValidationError

from common.tests.factories.extensions import create_approved_version
from common.tests.factories.files import FileFactory
from common.tests.factories.users import UserFactory

from constants.licenses import LICENSE_GPL3
from constants.base import EXTENSION_TYPE_CHOICES, EXTENSION_TYPE_SLUGS_SINGULAR
from extensions.models import Extension, Version, VersionPermission
from files.models import File
from files.validators import ManifestValidator

import tempfile
import shutil
import os
import zipfile
import toml

META_DATA = {
    "id": "an_id",
    "name": "Theme",
    "tagline": "A theme",
    "version": "0.1.0",
    "type": "theme",
    "license": [LICENSE_GPL3.slug],
    "blender_version_min": "4.2.0",
    "blender_version_max": "4.2.0",
    "schema_version": "1.0.0",
    "maintainer": "",
    "tags": [],
}


class CreateFileTest(TestCase):
    def setUp(self):
        super().setUp()

        self.user = UserFactory()
        self.temp_directory = tempfile.mkdtemp()

        file_data = {
            "name": "Blender Kitsu",
            "id": "blender_kitsu",
            "version": "0.1.5",
        }
        self.file = self._create_file_from_data("blender_kitsu_1.5.0.zip", file_data, self.user)

    def tearDown(self):
        super().tearDown()
        shutil.rmtree(self.temp_directory)

    @classmethod
    def _get_submit_url(cls):
        return reverse('extensions:submit')

    def _create_valid_extension(self, extension_id):
        return create_approved_version(
            extension__name='Blender Kitsu',
            extension__extension_id=extension_id,
            version='0.1.5-alpha+f52258de',
            file=FileFactory(
                type=File.TYPES.THEME,
                status=File.STATUSES.APPROVED,
            ),
        )

    def _create_file_from_data(self, filename, file_data, user):
        output_path = os.path.join(self.temp_directory, filename)
        manifest_path = os.path.join(self.temp_directory, "blender_manifest.toml")
        combined_meta_data = META_DATA.copy()
        combined_meta_data.update(file_data)

        version = combined_meta_data.get("version", "0.1.0")
        extension_id = combined_meta_data.get("id", "foobar").strip()

        with open(manifest_path, "w") as manifest_file:
            toml.dump(combined_meta_data, manifest_file)

        with zipfile.ZipFile(output_path, "w") as my_zip:
            arcname = f"{extension_id}-{version}/{os.path.basename(manifest_path)}"
            my_zip.write(manifest_path, arcname=arcname)

        os.remove(manifest_path)
        return output_path


class ValidateManifestTest(CreateFileTest):
    fixtures = ['licenses']

    def test_validation_manifest_extension_id_hyphen(self):
        self.assertEqual(Extension.objects.count(), 0)
        user = UserFactory()
        self.client.force_login(user)

        file_data = {
            "id": "id-with-hyphens",
        }

        bad_file = self._create_file_from_data("theme.zip", file_data, self.user)
        with open(bad_file, 'rb') as fp:
            response = self.client.post(
                self._get_submit_url(), {'source': fp, 'agreed_with_terms': True}
            )

        self.assertEqual(response.status_code, 200)
        self.assertDictEqual(
            response.context['form'].errors,
            {
                'source': [
                    'Invalid id from extension manifest: "id-with-hyphens". No hyphens are allowed.'
                ]
            },
        )

    def test_validation_manifest_extension_id_spaces(self):
        self.assertEqual(Extension.objects.count(), 0)
        user = UserFactory()
        self.client.force_login(user)

        file_data = {
            "id": "id with spaces",
        }

        bad_file = self._create_file_from_data("theme.zip", file_data, self.user)
        with open(bad_file, 'rb') as fp:
            response = self.client.post(
                self._get_submit_url(), {'source': fp, 'agreed_with_terms': True}
            )

        self.assertEqual(response.status_code, 200)
        self.assertDictEqual(
            response.context['form'].errors,
            {
                'source': [
                    'Invalid id from extension manifest: "id with spaces". Use a valid id consisting of Unicode letters, numbers or underscores.'
                ]
            },
        )

    def test_validation_manifest_extension_id_clash(self):
        """Test if we add two extensions with the same extension_id"""
        self.assertEqual(Extension.objects.count(), 0)
        self._create_valid_extension('blender_kitsu')
        self.assertEqual(Extension.objects.count(), 1)

        user = UserFactory()
        self.client.force_login(user)

        kitsu_1_5 = {
            "name": "Blender Kitsu",
            "id": "blender_kitsu",
            "version": "0.1.5",
        }

        extension_file = self._create_file_from_data("theme.zip", kitsu_1_5, self.user)
        with open(extension_file, 'rb') as fp:
            response = self.client.post(
                self._get_submit_url(), {'source': fp, 'agreed_with_terms': True}
            )

        self.assertEqual(response.status_code, 200)
        self.assertDictEqual(
            response.context['form'].errors,
            {
                'source': [
                    'The extension id in the manifest ("blender_kitsu") is already being used by another extension.'
                ]
            },
        )

    def test_validation_manifest_extension_id_mismatch(self):
        """Test if we try to add a new version to an extension with a mismatched extension_id"""
        self.assertEqual(Extension.objects.count(), 0)
        version = self._create_valid_extension('blender_kitsu')
        self.assertEqual(Extension.objects.count(), 1)

        # The same author is to send a new version to thte same extension
        self.client.force_login(version.file.user)

        non_kitsu_1_6 = {
            "name": "Blender Kitsu with a different ID",
            "id": "non_kitsu",
            "version": "0.1.6",
        }

        extension_file = self._create_file_from_data("theme.zip", non_kitsu_1_6, self.user)
        with open(extension_file, 'rb') as fp:
            response = self.client.post(
                version.extension.get_new_version_url(), {'source': fp, 'agreed_with_terms': True}
            )

        self.assertEqual(response.status_code, 200)
        self.assertDictEqual(
            response.context['form'].errors,
            {
                'source': [
                    'The extension id in the manifest ("non_kitsu") doesn\'t match the expected one for this extension ("blender_kitsu").'
                ]
            },
        )

    def test_validation_manifest_extension_id_repeated_version(self):
        """Test if we try to add a version to an extension without changing the version number"""
        self.assertEqual(Extension.objects.count(), 0)
        version = self._create_valid_extension('blender_kitsu')
        self.assertEqual(Extension.objects.count(), 1)

        # The same author is to send a new version to thte same extension
        self.client.force_login(version.file.user)

        kitsu_version_clash = {
            "name": "Change the name for lols",
            "id": version.extension.extension_id,
            "version": version.version,
        }

        extension_file = self._create_file_from_data(
            "kitsu_clash.zip", kitsu_version_clash, self.user
        )
        with open(extension_file, 'rb') as fp:
            response = self.client.post(
                version.extension.get_new_version_url(), {'source': fp, 'agreed_with_terms': True}
            )

        self.assertEqual(response.status_code, 200)
        self.assertDictEqual(
            response.context['form'].errors,
            {
                'source': [
                    f'The version {version.version} was already uploaded for this extension ({ version.extension.name})'
                ]
            },
        )


class ValidateManifestFields(TestCase):
    fixtures = ['licenses', 'version_permissions', 'tags']

    def setUp(self):
        self.mandatory_fields = {
            key: item.example for (key, item) in ManifestValidator.mandatory_fields.items()
        }
        self.optional_fields = {
            key: item.example for (key, item) in ManifestValidator.optional_fields.items()
        }

    def test_missing_fields(self):
        with self.assertRaises(ValidationError) as e:
            ManifestValidator({})
        self.assertEqual(
            e.exception.messages,
            [
                'The following values are missing from the manifest file: blender_version_min, id, license, maintainer, name, schema_version, tagline, type, version'
            ],
        )

    def test_type_examples(self):
        data = {
            **self.mandatory_fields,
            **self.optional_fields,
        }

        # It should run fine without any exceptions.
        ManifestValidator(data)

    def test_wrong_type_fields(self):
        data = {
            'blender_version_min': 1,
            'id': 1,
            'license': 'SPDX:GPL-3.0-or-later',
            'maintainer': 1,
            'name': 1,
            'schema_version': 1,
            'tagline': 1,
            'type': 1,
            'version': 1,
        }

        with self.assertRaises(ValidationError) as e:
            ManifestValidator(data)

        self.assertEqual(len(e.exception.messages), len(data.keys()))

        flatten_err_message = ''.join(e.exception.messages)
        for key in data.keys():
            self.assertIn(key, flatten_err_message)

    def test_wrong_optional_type_fields(self):
        testing_data = {
            'blender_version_max': 1,
            'website': 1,
            'copyright': 1,
            'permissions': 'network',
            'tags': 'Development',
            'website': 1,
        }
        complete_data = {
            **self.mandatory_fields,
            **testing_data,
        }

        with self.assertRaises(ValidationError) as e:
            ManifestValidator(complete_data)

        self.assertEqual(len(e.exception.messages), len(testing_data))

        flatten_err_message = ''.join(e.exception.messages)
        for key in testing_data.keys():
            self.assertIn(key, flatten_err_message)

    def test_blender_version_min(self):
        data = {
            **self.mandatory_fields,
            **self.optional_fields,
        }

        data['blender_version_min'] = '4.2.0'
        ManifestValidator(data)

        data['blender_version_min'] = '2.9'
        with self.assertRaises(ValidationError) as e:
            ManifestValidator(data)

        self.assertEqual(1, len(e.exception.messages))

        self.assertIn('blender_version_min', e.exception.messages[0])
        self.assertIn('4.2.0', e.exception.messages[0])

        data['blender_version_min'] = '4.1.0'
        with self.assertRaises(ValidationError) as e:
            ManifestValidator(data)
        self.assertEqual(
            e.exception.messages,
            [
                'Manifest value error: blender_version_min should be at least "4.2.0"',
            ],
        )

        data['blender_version_min'] = '4.2.0-beta'
        with self.assertRaises(ValidationError) as e:
            ManifestValidator(data)
        self.assertEqual(
            e.exception.messages,
            [
                'Manifest value error: blender_version_min should be at least "4.2.0"',
            ],
        )

    def test_licenses(self):
        data = {
            **self.mandatory_fields,
            **self.optional_fields,
        }

        data['license'] = ['SPDX:GPL-2.0-or-later']
        ManifestValidator(data)

        data['license'] = ['SPDX:GPL-1.0']
        with self.assertRaises(ValidationError) as e:
            ManifestValidator(data)

        message_begin = "Manifest value error: license expects a list of supported licenses, e.g.: ['SPDX:GPL-2.0-or-later']. Visit"
        self.assertIn(message_begin, e.exception.messages[0])

        data['license'] = ['SPDX:GPL-2.0-only']
        with self.assertRaises(ValidationError) as e:
            ManifestValidator(data)
        self.assertEqual(1, len(e.exception.messages))

    def test_tags(self):
        data = {
            **self.mandatory_fields,
            **self.optional_fields,
        }

        data['tags'] = ['Render']
        ManifestValidator(data)

        data['tags'] = ['UnsupportedTag']
        with self.assertRaises(ValidationError) as e:
            ManifestValidator(data)

        message_begin = "Manifest value error: tags expects a list of supported tags, e.g.: ['Animation', 'Sequencer']. Visit"
        self.assertIn(message_begin, e.exception.messages[0])

    def test_permissions(self):
        data = {
            **self.mandatory_fields,
            **self.optional_fields,
        }

        data['permissions'] = ['files']
        ManifestValidator(data)

        data.pop('permissions')
        ManifestValidator(data)

        data['permissions'] = ['non-supported-permission']
        with self.assertRaises(ValidationError) as e:
            ManifestValidator(data)

        message_begin = "Manifest value error: permissions expects a list of supported permissions, e.g.: ['files', 'network']. The supported permissions are: "
        self.assertIn(message_begin, e.exception.messages[0])

        # Check the licenses that will always be around.
        message_end = e.exception.messages[0][len(message_begin) :]
        self.assertIn('files', message_end)
        self.assertIn('network', message_end)

        # Check for any other license that we may add down the line.
        for permission in VersionPermission.objects.all():
            self.assertIn(permission.slug, message_end)

        data['permissions'] = []
        with self.assertRaises(ValidationError) as e:
            ManifestValidator(data)

        self.assertEqual(1, len(e.exception.messages))

    def test_tagline(self):
        data = {
            **self.mandatory_fields,
            **self.optional_fields,
        }

        data['tagline'] = "Short tag-line"
        ManifestValidator(data)

        data['tagline'] = ''
        with self.assertRaises(ValidationError) as e:
            ManifestValidator(data)
        self.assertEqual(1, len(e.exception.messages))
        self.assertIn('empty', e.exception.messages[0])

        for punctuation in ('.', '!', '?'):
            data['tagline'] = f'Tagline ending with a punctuation{punctuation}'
            with self.assertRaises(ValidationError) as e:
                ManifestValidator(data)
            self.assertEqual(1, len(e.exception.messages))
            self.assertIn('punctuation', e.exception.messages[0])

        data[
            'tagline'
        ] = 'Tagline that is suuper suuuuuper suuuuuuuuuuuuuuuuuuuuper suuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuper long'
        with self.assertRaises(ValidationError) as e:
            ManifestValidator(data)
        self.assertEqual(1, len(e.exception.messages))
        self.assertIn('too long', e.exception.messages[0])

    def test_type(self):
        data = {
            **self.mandatory_fields,
            **self.optional_fields,
        }

        # Good cops.
        data['type'] = EXTENSION_TYPE_SLUGS_SINGULAR[EXTENSION_TYPE_CHOICES.BPY]
        ManifestValidator(data)

        data['type'] = EXTENSION_TYPE_SLUGS_SINGULAR[EXTENSION_TYPE_CHOICES.THEME]
        ManifestValidator(data)

        # Bad cops.
        # keymap and asset bundle are not part of the initial release of the extensions site.
        for type in ('keymap', 'asset-bundle', 'another-random-unsupported-type'):
            data['type'] = type
            with self.assertRaises(ValidationError) as e:
                ManifestValidator(data)
                self.assertEqual(1, len(e.exception.messages))

        data['type'] = ""
        with self.assertRaises(ValidationError) as e:
            ManifestValidator(data)
            self.assertEqual(1, len(e.exception.messages))


class VersionPermissionsTest(CreateFileTest):
    fixtures = ['licenses', 'version_permissions']

    def _test_version_permission(self, permissions):
        self.assertEqual(Extension.objects.count(), 0)
        version_original = self._create_valid_extension('blender_kitsu')
        extension = version_original.extension
        self.assertEqual(Extension.objects.count(), 1)
        self.assertEqual(Version.objects.count(), 1)
        self.assertEqual(version_original, extension.latest_version)

        # The same author is to send a new version to the same extension but with a new permission
        self.client.force_login(version_original.file.user)

        new_kitsu = {
            "id": "blender_kitsu",
            "version": "0.1.6",
            "permissions": permissions,
        }

        # Step 1: submit the file
        extension_file = self._create_file_from_data("kitsu-0.1.6.zip", new_kitsu, self.user)
        with open(extension_file, 'rb') as fp:
            response = self.client.post(
                extension.get_new_version_url(), {'source': fp, 'agreed_with_terms': True}
            )
        self.assertEqual(response.status_code, 302)

        # Although this creates the file, the version is not created until
        # we click on Submit for Review.

        # Check step 2: finalise new version and send to review
        url = response['Location']
        response = self.client.post(
            url,
            {
                'license': LICENSE_GPL3.id,
                'version': '2.0.1',
                'blender_version_min': '2.93.0',
                'blender_version_max': '3.2.0',
            },
        )
        self.assertEqual(response.status_code, 302)

        # The version needs to be approved.
        _file = File.objects.filter(original_name='kitsu-0.1.6.zip').first()
        _file.status = File.STATUSES.APPROVED
        _file.save()

        self.assertNotEqual(version_original, extension.latest_version)
        self.assertEqual(Extension.objects.count(), 1)
        self.assertEqual(Version.objects.count(), 2)

        return extension.latest_version

    def test_version_permission_register(self):
        """Make sure permissions are saved"""
        permissions = ['network']
        latest_version = self._test_version_permission(permissions)
        self.assertEqual(latest_version.permissions.count(), 1)
        self.assertEqual(latest_version.permissions.first().slug, 'network')
