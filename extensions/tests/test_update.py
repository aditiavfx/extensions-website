from pathlib import Path

from django.test import TestCase

from common.tests.factories.extensions import create_approved_version
from common.tests.utils import _get_all_form_errors
from files.models import File

TEST_FILES_DIR = Path(__file__).resolve().parent / 'files'
POST_DATA = {
    'name': ['Customizable homogeneous emulation'],
    'description': ['Long Lorem Ipsum Blady Blarem'],
    'tagline': ['Short description'],
    'support': ['https://example.com/bar/'],
    'website': ['https://example.com/home/'],
    'tags': ['easing, keyframe'],
    'preview_set-TOTAL_FORMS': ['0'],
    'preview_set-INITIAL_FORMS': ['0'],
    'preview_set-MIN_NUM_FORMS': ['0'],
    'preview_set-MAX_NUM_FORMS': ['1000'],
    'preview_set-0-id': [''],
    # 'preview_set-0-extension': [str(extension.pk)],
    'preview_set-1-id': [''],
    # 'preview_set-1-extension': [str(extension.pk)],
    'form-TOTAL_FORMS': ['0'],
    'form-INITIAL_FORMS': ['0'],
    'form-MIN_NUM_FORMS': ['0'],
    'form-MAX_NUM_FORMS': ['1000'],
    'form-0-id': '',
    'form-0-caption': ['First Preview Caption Text'],
    'form-1-id': '',
    'form-1-caption': ['Second Preview Caption Text'],
}


class UpdateTest(TestCase):
    fixtures = ['dev', 'tags', 'licenses']

    def test_get_manage_page(self):
        extension = create_approved_version().extension

        url = extension.get_manage_url()
        user = extension.authors.first()
        self.client.force_login(user)
        response = self.client.get(url)

        self.assertEqual(response.status_code, 200)
        # TODO: check that all of the most important data is present on the page

    def test_post_upload_a_preview_image(self):
        extension = create_approved_version().extension

        data = {
            **POST_DATA,
            'form-TOTAL_FORMS': ['1'],
        }
        file_name1 = 'test_preview_image_0001.png'
        url = extension.get_manage_url()
        user = extension.authors.first()
        self.client.force_login(user)
        with open(TEST_FILES_DIR / file_name1, 'rb') as fp1:
            files = {
                'form-0-source': fp1,
            }
            response = self.client.post(url, {**data, **files})

        self.assertEqual(
            response.status_code,
            302,
            _get_all_form_errors(response),
        )
        self.assertEqual(response['Location'], url)
        extension.refresh_from_db()
        self.assertEqual(File.objects.filter(type=File.TYPES.IMAGE).count(), 1)
        self.assertEqual(extension.previews.count(), 1)
        file1 = extension.previews.all()[0]
        self.assertEqual(file1.extension_preview.first().caption, 'First Preview Caption Text')
        self.assertEqual(
            file1.original_hash,
            'sha256:643e15eb6c4831173bbcf71b8c85efc70cf3437321bf2559b39aa5e9acfd5340',
        )
        self.assertEqual(
            file1.hash, 'sha256:643e15eb6c4831173bbcf71b8c85efc70cf3437321bf2559b39aa5e9acfd5340'
        )
        self.assertEqual(file1.original_name, file_name1)
        self.assertEqual(file1.size_bytes, 1163)
        self.assertTrue(
            file1.source.url.startswith(
                '/media/images/64/643e15eb6c4831173bbcf71b8c85efc70cf3437321bf2559b39aa5e9acfd5340'
            )
        )
        self.assertTrue(file1.source.url.endswith('.png'))
        self.assertEqual(file1.user_id, user.pk)

    def test_post_upload_multiple_preview_images(self):
        extension = create_approved_version().extension

        data = {
            **POST_DATA,
            'form-TOTAL_FORMS': ['2'],
        }
        file_name1 = 'test_preview_image_0001.png'
        file_name2 = 'test_preview_image_0002.png'
        url = extension.get_manage_url()
        user = extension.authors.first()
        self.client.force_login(user)
        with open(TEST_FILES_DIR / file_name1, 'rb') as fp1, open(
            TEST_FILES_DIR / file_name2, 'rb'
        ) as fp2:
            files = {
                'form-0-source': fp1,
                'form-1-source': fp2,
            }
            response = self.client.post(
                url,
                {**data, **files},
            )

        self.assertEqual(response.status_code, 302, _get_all_form_errors(response))
        self.assertEqual(response['Location'], url)
        extension.refresh_from_db()
        self.assertEqual(File.objects.filter(type=File.TYPES.IMAGE).count(), 2)
        self.assertEqual(extension.previews.count(), 2)
        file1 = extension.previews.all()[0]
        file2 = extension.previews.all()[1]
        self.assertEqual(file1.extension_preview.first().caption, 'First Preview Caption Text')
        self.assertEqual(file2.extension_preview.first().caption, 'Second Preview Caption Text')
        self.assertEqual(
            file1.original_hash,
            'sha256:643e15eb6c4831173bbcf71b8c85efc70cf3437321bf2559b39aa5e9acfd5340',
        )
        self.assertEqual(
            file1.hash, 'sha256:643e15eb6c4831173bbcf71b8c85efc70cf3437321bf2559b39aa5e9acfd5340'
        )
        self.assertEqual(
            file2.original_hash,
            'sha256:f8ef448d66e2506055e2586d4cb20dc8758b19cd6e8b052231fcbcad2e2be4b3',
        )
        self.assertEqual(
            file2.hash, 'sha256:f8ef448d66e2506055e2586d4cb20dc8758b19cd6e8b052231fcbcad2e2be4b3'
        )
        self.assertEqual(file1.original_name, file_name1)
        self.assertEqual(file2.original_name, file_name2)
        self.assertEqual(file1.size_bytes, 1163)
        self.assertEqual(file2.size_bytes, 1693)
        self.assertTrue(
            file1.source.url.startswith(
                '/media/images/64/643e15eb6c4831173bbcf71b8c85efc70cf3437321bf2559b39aa5e9acfd5340',
            )
        )
        self.assertTrue(file1.source.url.endswith('.png'))
        self.assertTrue(
            file2.source.url.startswith(
                '/media/images/f8/f8ef448d66e2506055e2586d4cb20dc8758b19cd6e8b052231fcbcad2e2be4b3',
            )
        )
        self.assertTrue(file2.source.url.endswith('.png'))
        for f in (file1, file2):
            self.assertEqual(f.user_id, user.pk)

    def test_post_upload_validation_errors(self):
        extension = create_approved_version().extension

        url = extension.get_manage_url()
        user = extension.authors.first()
        self.client.force_login(user)

        response = self.client.post(url, {})

        self.assertEqual(response.status_code, 200)
        self.assertDictEqual(
            response.context['form'].errors,
            {
                'description': ['This field is required.'],
            },
            _get_all_form_errors(response),
        )
        self.assertFalse("TODO: It should also list previews as required")

    def test_post_upload_validation_error_duplicate_images(self):
        extension = create_approved_version().extension

        data = {
            **POST_DATA,
            'form-TOTAL_FORMS': ['2'],
        }
        file_name1 = 'test_preview_image_0001.png'
        file_name2 = 'test_preview_image_0003.png'
        url = extension.get_manage_url()
        user = extension.authors.first()
        self.client.force_login(user)
        with open(TEST_FILES_DIR / file_name1, 'rb') as fp1, open(
            TEST_FILES_DIR / file_name2, 'rb'
        ) as fp2:
            files = {
                'form-0-source': fp1,
                'form-1-source': fp2,
            }
            response = self.client.post(url, {**data, **files})

        self.assertEqual(response.status_code, 200)
        self.assertEqual(
            response.context['add_preview_formset'].forms[0].errors,
            {'source': ['File with this Hash already exists.']},
        )
