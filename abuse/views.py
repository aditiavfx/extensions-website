import logging

from django.contrib.auth.mixins import UserPassesTestMixin, LoginRequiredMixin
from django.http import Http404
from django.views.generic import DetailView
from django.views.generic.edit import CreateView

from .forms import ReportForm
from abuse.models import AbuseReport
import extensions.views.mixins

log = logging.getLogger(__name__)


class ReportExtensionView(
    LoginRequiredMixin,
    extensions.views.mixins.ListedExtensionMixin,
    UserPassesTestMixin,
    CreateView,
):
    model = AbuseReport
    form_class = ReportForm

    def test_func(self) -> bool:
        # TODO: best to redirect to existing report or show a friendly message
        return not AbuseReport.exists(user_id=self.request.user.pk, extension_id=self.extension.id)

    def form_valid(self, form):
        """Link newly created rating to latest version and current user."""
        form.instance.reporter = self.request.user
        form.instance.extension = self.extension
        form.instance.extension_version = self.extension.latest_version.version
        return super().form_valid(form)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['extension'] = self.extension
        return context

    def get_success_url(self) -> str:
        return self.object.get_absolute_url()


class ReportView(LoginRequiredMixin, DetailView):
    model = AbuseReport

    def get_object(self, *args, **kwargs):
        obj = super().get_object(*args, **kwargs)
        if obj.reporter.pk != self.request.user.pk and not self.request.user.is_staff:
            raise Http404()
        return obj
