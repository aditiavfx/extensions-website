from django import forms
import reviewers.models


class CommentForm(forms.ModelForm):
    class Meta:
        model = reviewers.models.ApprovalActivity
        fields = ('message', 'type')
        widgets = {
            'message': forms.Textarea(attrs={'placeholder': 'Your message'}),
        }

    def __init__(self, *args, **kwargs):
        self.base_fields['message'].widget.attrs.update({'placeholder': 'Add your comment here...'})
        super().__init__(*args, **kwargs)
