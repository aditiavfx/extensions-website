[
{
  "model": "sites.site",
  "pk": 1,
  "fields": {
    "domain": "extensions.local:8111",
    "name": "Blender Extensions Dev"
  }
},
{
  "model": "flatpages.flatpage",
  "pk": 1,
  "fields": {
    "url": "/conditions-of-use/",
    "title": "Conditions of Use",
    "content": "You may not use any of Blender online services to:\r\n\r\n * Do anything illegal or otherwise violate applicable law,\r\n\r\n * Threaten, harass, or violate the privacy rights of others; send unsolicited communications; or intercept, monitor, or modify communications not intended for you,\r\n\r\n * Harm users such as by using viruses, spyware or malware, worms, trojan horses, time bombs or any other such malicious codes or instructions,\r\n\r\n * Deceive, mislead, defraud, phish, or commit or attempt to commit identity theft,\r\n\r\n * Engage in or promote illegal gambling,\r\n\r\n * Degrade, intimidate, incite violence against, or encourage prejudicial action against someone or a group based on age, gender, race, ethnicity, national origin, religion, sexual orientation, disability, geographic location or other protected category,\r\n\r\n * Exploit or harm children,\r\n\r\n * Sell, purchase, or advertise illegal or controlled products or services,\r\n\r\n * Upload, download, transmit, display, or grant access to content that includes graphic depictions of sexuality or violence,\r\n\r\n * Collect or harvest personally identifiable information without permission. This includes, but is not limited to, account names and email addresses,\r\n\r\n * Engage in any activity that interferes with or disrupts Blender’s online services or products (or the servers and networks which are connected to Blender’s services),\r\n\r\n * Violate the copyright, trademark, patent, or other intellectual property rights of others,\r\n\r\n * Violate any person’s rights of privacy or publicity.\r\n\r\nYou may not use any Blender online service in a way that violates the Conditions of Use or license that applies to the particular service.\r\n\r\nThese are only examples. You should not consider this a complete list, and we may update the list from time to time. Blender Institute reserves the right to remove any content or suspend any users that it deems in violation of these conditions.",
    "enable_comments": false,
    "template_name": "",
    "registration_required": false,
    "sites": [
      1
    ]
  }
},
{
  "model": "flatpages.flatpage",
  "pk": 2,
  "fields": {
    "url": "/policies/",
    "title": "Extensions Policies",
    "content": "# Free and Open Source\r\nUploaded extensions must be wholly compatible with [GNU General Public License, version 2](https://www.gnu.org/licenses/old-licenses/gpl-2.0.html) or [GNU General Public License, version 3](https://www.gnu.org/licenses/gpl-3.0.html).\r\n\r\n# No surprises\r\nAn extension must have an easy-to-read description mentioning everything it does.\r\n\r\nIt must be abundantly clear from the extension’s listing what functionality it offers, and Blender users should not be presented with unexpected experiences when they download and install the extension.\r\n\r\n# Content\r\n\r\n* Extension listings must comply with Blender services [conditions of use](/conditions-of-use/).\r\n\r\n* Extensions offered on extensions.blender.org should be fully functional, documented and actively maintained products.\r\n\r\n* Extensions should not require any external functional components (other than Blender itself) that need to be downloaded from elsewhere.\r\n \r\n* Extensions that (need to) connect to Internet services outside of blender.org ecosystem, should offer access to these sites without additional restrictions (such as login or registration).\r\n\r\n* If the extension is a fork of another extension, the name must clearly distinguish it from the original and provide a significant difference in functionality and/or code.\r\n\r\n* Extensions that are intended for internal or private use or are only accessible to a closed user group may not be listed on extensions.blender.org.\r\n\r\n* Linking to external funding platforms in the description is allowed.\r\n\r\n   However, it is not allowed to have additional requirements (such as registration, payments or keys) blocking functionality of the extension.\r\n\r\n# Acceptable code practices\r\n\r\n* Extension code must be reviewable: no obfuscated code or byte code is allowed.\r\n\r\n* Extension must be self-contained and not load remote code for execution.\r\n\r\n* Extension must not send data to any remote locations without authorization from the user.\r\n\r\n* Extension should avoid including redundant code or files.\r\n\r\n* Extension must not negatively impact performance or stability of Blender.\r\n\r\n* Add-on's code must be compliant with the [guidelines](https://developer.blender.org/docs/handbook/addons/guidelines/).",
    "enable_comments": false,
    "template_name": "",
    "registration_required": false,
    "sites": [
      1
    ]
  }
},
{
  "model": "flatpages.flatpage",
  "pk": 3,
  "fields": {
    "url": "/about/",
    "title": "About",
    "content": "# Blender Extensions Platform\n\nThe Blender Extensions platform is the online directory of free and Open Source extensions for Blender.\n\nThe goal of this platform is to make it easy for Blender users to find and share their add-ons and themes, entirely within the Free and Open Source spirit.\n\nThe platform only offers [**GNU GPL compliant software**](https://www.gnu.org/licenses/old-licenses/gpl-2.0.en.html).\n\n## How to get started\n\n 1. Download a recent daily build of [Blender 4.2 Apha](https://builder.blender.org).\n 2. Enable the [experimental feature](https://docs.blender.org/manual/en/latest/editors/preferences/experimental.html): Prototypes → Extensions.\n\nNow you can should be able to browse and install extensions in Blender:\n\n![User Preferences →  Extensions](https://code.blender.org/wp-content/uploads/2024/02/about-1.jpg)\n\nYou can also explore the extensions in this web-site and install them via drag & dropping into Blender.\n\n### Follow these 5 simple steps\n\n1. Find an extension that suits you click on the **Get** button.\n\n!['Get Add-on' button](https://code.blender.org/wp-content/uploads/2024/02/about-2.jpg)\n\n2. Drag the button out of the website ...\n\n!['Drag and Drop into Blender' button](https://code.blender.org/wp-content/uploads/2024/02/about-3.jpg)\n\n3. ... into Blender.\n\n![Link dragged into Blender](https://code.blender.org/wp-content/uploads/2024/02/about-4.jpg)\n\n4. You will be prompted about installing and enabling the extension. Confirm to download it.\n\n![Install & Enable](https://code.blender.org/wp-content/uploads/2024/02/about-5.jpg)\n\n5. Enjoy your newly installed extension\n\n## How to publish an extension\n\n* Read the [Conditions of Use](/conditions-of-use/) and [Policies](/policies/).\n* Follow the [guidelines in the User Manual](https://docs.blender.org/manual/en/dev/extensions/index.html#how-to-create-extensions).\n* [Upload your Extension](/submit/).\n* [Wait for Review](/approval-queue/).\n\n## See also\n\n* [Conditions of Use](/conditions-of-use/)\n* [Extensions Policies](/policies/)\n* [Report a Bug](https://projects.blender.org/infrastructure/extensions-website/issues)",
    "enable_comments": false,
    "template_name": "",
    "registration_required": false,
    "sites": [
      1
    ]
  }
}
]
