"""Write stats data."""
import logging

from django.contrib.auth import get_user_model
from django.core.management.base import BaseCommand

from stats.models import ExtensionView, ExtensionDownload, VersionDownload

logger = logging.getLogger('write_stats')
logger.setLevel(logging.DEBUG)
User = get_user_model()


class Command(BaseCommand):
    """Write stats data."""

    def write_extension_counts(self):
        """Calculate view and download counts for Extensions."""
        ExtensionView.update_counters(to_field='view_count')
        ExtensionDownload.update_counters(to_field='download_count')
        VersionDownload.update_counters(to_field='download_count')

    def handle(self, *args, **options):
        """Write various stats."""
        self.write_extension_counts()
