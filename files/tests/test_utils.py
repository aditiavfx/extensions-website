from django.test import TestCase

from files.utils import find_file_inside_zip_list


class UtilsTest(TestCase):
    manifest = 'blender_manifest.toml'

    def test_find_manifest(self):
        name_list = [
            "blender_manifest.toml",
        ]
        manifest_file = find_file_inside_zip_list(self.manifest, name_list)
        self.assertEqual(manifest_file, "blender_manifest.toml")

    def test_find_manifest_nested(self):
        name_list = [
            "foobar-1.0.3/",
            "foobar-1.0.3/bar.txt",
            "foobar-1.0.3/foo.txt",
            "foobar-1.0.3/another_manifest.toml",
            "foobar-1.0.3/blender_manifest.toml",
            "foobar-1.0.3/manifest.toml",
            "foobar-1.0.3/manifest.json",
        ]
        manifest_file = find_file_inside_zip_list(self.manifest, name_list)
        self.assertEqual(manifest_file, "foobar-1.0.3/blender_manifest.toml")

    def test_find_manifest_no_zipped_folder(self):
        name_list = [
            "foobar-1.0.3/blender_manifest.toml",
        ]
        manifest_file = find_file_inside_zip_list(self.manifest, name_list)
        self.assertEqual(manifest_file, "foobar-1.0.3/blender_manifest.toml")

    def test_find_manifest_no_manifest(self):
        name_list = [
            "foobar-1.0.3/",
        ]
        manifest_file = find_file_inside_zip_list(self.manifest, name_list)
        self.assertEqual(manifest_file, None)

    def test_find_manifest_with_space(self):
        name_list = [
            "foobar-1.0.3/ blender_manifest.toml",
            "foobar-1.0.3/_blender_manifest.toml",
            "foobar-1.0.3/blender_manifest.toml.txt",
            "blender_manifest.toml/my_files.py",
        ]
        manifest_file = find_file_inside_zip_list(self.manifest, name_list)
        self.assertEqual(manifest_file, None)
