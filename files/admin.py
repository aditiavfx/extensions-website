from django.contrib import admin

from .models import File


@admin.register(File)
class FileAdmin(admin.ModelAdmin):
    view_on_site = False
    save_on_top = True

    list_filter = (
        'type',
        'status',
        'date_status_changed',
        'date_approved',
        'date_deleted',
    )
    list_display = ('original_name', 'date_created', 'type', 'status')

    list_select_related = ('version__extension', 'user')

    readonly_fields = (
        'id',
        'date_created',
        'date_deleted',
        'date_modified',
        'date_approved',
        'date_status_changed',
        'size_bytes',
        'user',
        'original_hash',
        'original_name',
        'hash',
        'content_type',
    )
    search_fields = (
        '^version__extension__slug',
        '^version__extension__name',
        'extensions__slug',
        'extensions__name',
    )

    fieldsets = (
        (
            None,
            {
                'fields': (
                    'id',
                    ('source', 'thumbnail'),
                    ('original_name', 'content_type'),
                    'type',
                    'status',
                )
            },
        ),
        (
            'Dates',
            {
                'fields': (
                    'date_created',
                    'date_modified',
                    'date_status_changed',
                    'date_approved',
                    'date_deleted',
                )
            },
        ),
        (
            'Details',
            {
                'fields': (
                    ('hash', 'original_hash'),
                    'metadata',
                    'size_bytes',
                    'user',
                ),
            },
        ),
    )
