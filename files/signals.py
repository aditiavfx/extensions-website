from django.db.models.signals import pre_save
from django.dispatch import receiver

import files.models


@receiver(pre_save, sender=files.models.File)
def _record_changes(sender: object, instance: files.models.File, **kwargs: object) -> None:
    was_changed, old_state = instance.pre_save_record()

    instance.record_status_change(was_changed, old_state, **kwargs)
