import logging

from django.contrib.auth import get_user_model
from django.db import models
from django.urls import reverse

from common.model_mixins import CreatedModifiedMixin
from constants.base import (
    TEAM_ROLE_CHOICES,
    TEAM_ROLE_MANAGER,
    TEAM_ROLE_MEMBER,
)
import utils

User = get_user_model()
logger = logging.getLogger(__name__)


class Team(CreatedModifiedMixin, models.Model):
    slug = models.SlugField(unique=True, null=False, blank=False, editable=True)
    name = models.CharField(max_length=128, null=False, blank=False)
    users = models.ManyToManyField(User, through='TeamsUsers', related_name='teams')

    def clean(self) -> None:
        if not self.slug:
            self.slug = utils.slugify(self.name)
        super().clean()

    def __str__(self) -> str:
        return f'<Team {self.name}>'

    def save(self, *args, **kwargs):
        self.clean()
        return super().save(*args, **kwargs)

    def get_absolute_url(self) -> str:
        return reverse('extensions:by-team', kwargs={'team_slug': self.slug})


class TeamsUsers(CreatedModifiedMixin, models.Model):
    class Meta:
        verbose_name = 'Team member'
        verbose_name_plural = 'Team members'

    user = models.ForeignKey(User, on_delete=models.CASCADE, related_name='team_users')
    role = models.SmallIntegerField(default=TEAM_ROLE_MEMBER, choices=TEAM_ROLE_CHOICES)
    team = models.ForeignKey(Team, on_delete=models.CASCADE, related_name='team_users')

    @property
    def is_manager(self) -> bool:
        return self.role == TEAM_ROLE_MANAGER
