from django.contrib import admin

import teams.models


class TeamsUsersInline(admin.TabularInline):
    model = teams.models.TeamsUsers
    raw_id_fields = ('user',)
    extra = 0


@admin.register(teams.models.Team)
class TeamAdmin(admin.ModelAdmin):
    inlines = [TeamsUsersInline]
    prepopulated_fields = {'slug': ('name',)}
    list_display = ('date_created', 'name')
    view_on_site = True
