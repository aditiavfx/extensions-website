server {
    server_name {{ domain }};
    # Avoid DisallowedHost "Invalid HTTP_HOST header" errors
    if ($host !~* ^({{ domain }})$) {
        return 444;
    }

    access_log /var/log/nginx/{{ domain }}-access.log;
    error_log /var/log/nginx/{{ domain }}-error.log;

    client_max_body_size {{ client_max_body_size }};

    location / {
        proxy_set_header Host $host;

        proxy_pass http://127.0.0.1:{{ port }}/;
        proxy_connect_timeout 600s;
        proxy_read_timeout 600s;
        proxy_send_timeout 600s;
        proxy_ssl_name $host;
        proxy_ssl_server_name on;
        send_timeout 600s;
    }

    error_page 403 /errors/403.html;
    error_page 404 /errors/404.html;
    error_page 405 /errors/405.html;
    error_page 500 501 502 503 504 /errors/5xx.html;
    location ^~ /errors/ {
        alias {{ dir.errors | regex_replace('\\/*$', '/') }};
        internal;
    }

    location /media/  {
        alias {{ dir.media | regex_replace('\\/*$', '/') }};
    }
    location /static/  {
        alias {{ dir.static | regex_replace('\\/*$', '/') }};
    }
}
