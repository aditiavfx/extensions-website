from django.utils.translation import gettext_lazy as _
from extended_choices import Choices

# Extension author roles.
AUTHOR_ROLE_DEV = 1
AUTHOR_ROLE_CHOICES = ((AUTHOR_ROLE_DEV, _('Developer')),)

# Extension types
EXTENSION_TYPE_CHOICES = Choices(
    ('BPY', 1, _('Add-on')),
    ('THEME', 2, _('Theme')),
)
STATUS_INCOMPLETE = 1
STATUS_AWAITING_REVIEW = 2
STATUS_APPROVED = 3
STATUS_DISABLED = 4
STATUS_DISABLED_BY_AUTHOR = 5

# Extension statuses
EXTENSION_STATUS_CHOICES = Choices(
    ('INCOMPLETE', STATUS_INCOMPLETE, _('Incomplete')),
    ('AWAITING_REVIEW', STATUS_AWAITING_REVIEW, _('Awaiting Review')),
    ('APPROVED', STATUS_APPROVED, _('Approved')),
    ('DISABLED', STATUS_DISABLED, _('Disabled by staff')),
    ('DISABLED_BY_AUTHOR', STATUS_DISABLED_BY_AUTHOR, _('Disabled by author')),
)

# File types
FILE_TYPE_CHOICES = Choices(
    ('BPY', 1, _('Add-on')),
    ('THEME', 2, _('Theme')),
    # ('KEYMAP', 3, _('Keymap')),
    # ('ASSET_BUNDLE', 4, _('Asset Bundle')),
    ('IMAGE', 5, _('Image')),
    ('VIDEO', 6, _('Video')),
)

# File statuses
FILE_STATUS_CHOICES = Choices(
    ('AWAITING_REVIEW', STATUS_AWAITING_REVIEW, _('Awaiting Review')),
    ('APPROVED', STATUS_APPROVED, _('Approved')),
    ('DISABLED', STATUS_DISABLED, _('Disabled by staff')),
    ('DISABLED_BY_AUTHOR', STATUS_DISABLED_BY_AUTHOR, _('Disabled by author')),
)

# We use these slugs in browse page urls.
EXTENSION_TYPE_SLUGS = {
    EXTENSION_TYPE_CHOICES.BPY: 'add-ons',
    EXTENSION_TYPE_CHOICES.THEME: 'themes',
}
# We use these slugs in the JSON.
EXTENSION_TYPE_SLUGS_SINGULAR = {
    EXTENSION_TYPE_CHOICES.BPY: 'add-on',
    EXTENSION_TYPE_CHOICES.THEME: 'theme',
}
EXTENSION_TYPE_PLURAL = {
    EXTENSION_TYPE_CHOICES.BPY: _('Add-ons'),
    EXTENSION_TYPE_CHOICES.THEME: _('Themes'),
}
EXTENSION_SLUGS_PATH = '|'.join(EXTENSION_TYPE_SLUGS.values())

VALID_SOURCE_EXTENSIONS = ('zip',)

# Rating scores
RATING_SCORE_CHOICES = Choices(
    ('FIVE_STARS', 5, '★★★★★'),
    ('FOUR_STARS', 4, '★★★★'),
    ('THREE_STARS', 3, '★★★'),
    ('TWO_STARS', 2, '★★'),
    ('ONE_STAR', 1, '★'),
)

# Rating statuses
RATING_STATUS_CHOICES = Choices(
    ('AWAITING_REVIEW', STATUS_AWAITING_REVIEW, _('Awaiting Review')),
    ('APPROVED', STATUS_APPROVED, _('Approved')),
    ('REJECTED', STATUS_DISABLED, _('Rejected by staff')),
)

# Team roles.
TEAM_ROLE_MEMBER = 1
TEAM_ROLE_MANAGER = 2
TEAM_ROLE_CHOICES = (
    (TEAM_ROLE_MEMBER, _('Member')),
    (TEAM_ROLE_MANAGER, _('Manager')),
)
